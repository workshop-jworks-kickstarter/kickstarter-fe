FROM alpine:3.3
MAINTAINER Vierbergen.Tim@gmail.com

RUN apk --update \
    add lighttpd && \
    rm -rf /var/cache/apk/* && \
    adduser www-data -G www-data -H -s /bin/false -D

RUN ln -sf /dev/stderr  /var/log/lighttpd/access.log &&\
    ln -sf /dev/stderr  /var/log/lighttpd/error.log

ADD docker/lighttpd.conf /etc/lighttpd/lighttpd.conf
COPY dist/kickstarter-fe /var/www/

EXPOSE 4200

ENTRYPOINT ["/usr/sbin/lighttpd","-D","-f", "/etc/lighttpd/lighttpd.conf"]
