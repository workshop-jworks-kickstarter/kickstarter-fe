#!/bin/bash

set -x
export TAG=$1
export TEST_PORT=$2
# echo "Stopping test env for build ${TAG}"
docker-compose down -v

# clean up
# sudo rm -rf $userDirPath/node-red/lib
