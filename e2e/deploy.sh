#!/bin/bash

set -x
export TAG=$1
export TEST_PORT=$2
# export userDirPath=`pwd`
# echo "Starting test env for build ${TAG}"
# echo "Node-RED is using ${userDirPath}/node-red as mapped data source"
# sudo chmod -R 777 node-red
docker-compose up -d
