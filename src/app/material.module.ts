import { NgModule } from '@angular/core';
import { MatButtonModule, MatIconModule, MatSidenavModule, MatListModule, MatMenuModule,
  MatToolbarModule, MatTableModule, MatPaginatorModule, MatSortModule, MatCardModule,
  MatCheckbox, MatCheckboxModule, MatInputModule, MatDialogModule } from '@angular/material';


@NgModule({
  declarations: [
      ],
  imports: [
    MatButtonModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatMenuModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatCardModule,
    MatCheckboxModule,
    MatInputModule,
    MatDialogModule
  ],
  exports: [
    MatButtonModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatMenuModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatCardModule,
    MatCheckboxModule,
    MatInputModule,
    MatDialogModule
  ]
})
export class MaterialModule { }
