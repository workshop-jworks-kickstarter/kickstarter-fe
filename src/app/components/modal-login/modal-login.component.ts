import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


export interface DialogData {
  username: string;
  info: string;
}

@Component({
  selector: 'jworks-modal-login',
  templateUrl: './modal-login.component.html',
  styleUrls: ['./modal-login.component.scss']
})
export class ModalLoginComponent implements OnInit {
    
  constructor(
      public dialogRef: MatDialogRef<ModalLoginComponent>,
      @Inject(MAT_DIALOG_DATA) public data: DialogData) {

  }
  
  ngOnInit() {

  }
  
}