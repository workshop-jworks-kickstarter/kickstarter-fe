import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ModalLoginComponent, DialogData } from './components/modal-login/modal-login.component';
import { ApiService } from './services/api.service';
import { tap, map } from 'rxjs/operators';

@Component({
  selector: 'jworks-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'kickstarter-fe';

  username: string;
  data: DialogData;

  constructor(public dialog: MatDialog,
    private service: ApiService) {

  }

  onLogin() {
    this.service.login(this.username).pipe(
      tap((data: DialogData) => {
        this.data = data;
      })
    ).subscribe(() => {
      this.openDialog();
    })
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ModalLoginComponent, {
      width: '250px',
      data: this.data
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}
