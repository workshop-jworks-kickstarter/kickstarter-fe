import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { DialogData } from '../components/modal-login/modal-login.component';
import { MAT_DIALOG_DATA } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }


  login(username: string): Observable<DialogData> {
    // const data: DialogData = {
    //   info: "heel wat info",
    //   username: "aUsername"
    // };
    return this.http.get<DialogData>('api/login/' + username).pipe(
      tap((data) => {
        console.log(data);
      })
    );
    
    
  }


}
